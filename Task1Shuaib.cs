using System;
using System.Collections.Generic;

namespace BootCampTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> BootCamp = new Dictionary<int, string>();
            BootCamp.Add(0, "Shuaib");
            BootCamp.Add(1, "Aaron");
            BootCamp.Add(2, "Poonam");
            BootCamp.Add(3, "Velvyn");
            BootCamp.Add(4, "Anjita");
            BootCamp.Add(5, "Devarsh");
            BootCamp.Add(6, "Vaibhavi");
            BootCamp.Add(7, "Sagar");
            BootCamp.Add(8, "Chetan");
            Console.WriteLine("Following are the students at Boot Camp:\n");
            foreach (KeyValuePair<int, string> student in BootCamp)
            {
                Console.WriteLine( student.Value);
            }
            Random rand = new Random();
            int knight = rand.Next(9);
            Console.WriteLine("A knight has been randomly chosen from amongst the students. Guess it right to break the loop!\n");
            int i = 0;
            while(i==0)
            {
                String Guess = Console.ReadLine();
                if (Guess.Equals(BootCamp[knight]))
                {
                    i = 1;
                    Console.WriteLine("Correct guess!");
                }
                else
                    Console.WriteLine("Try again!");
            }
        }
    }
}
