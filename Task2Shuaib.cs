using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            while(i!=7)
            {
                Console.WriteLine("\nWhat would you like to see?\n1. Dot\n2. Line\n3. Triangle\n4. Rhombus\n5. Pentagon\n6. Hexagon\n7. Exit\n");
                String temp = Console.ReadLine();
                i = Convert.ToInt32(temp);
                switch(i)
                {
                    case 1:
                        Console.WriteLine(".\n");
                        break;

                    case 2:
                        Console.WriteLine("____________________________________________________\n");
                        break;

                    case 3:
                        int c2 = 9;
                        int e2 = 1;
                        for (int a2 = 0; a2 < 10; a2++)
                        {

                            for (int b2 = 0; b2 < c2; b2++)
                            {

                                Console.Write(" ");
                            }
                            c2--;
                            for (int d2 = 0; d2 < e2; d2++)
                            {

                                Console.Write("#");
                            }
                            e2 += 2;
                            Console.Write("\n");
                        }
                        break;

                    case 4:
                        int c = 9;
                        int e = 1;
                        int o = 1;
                        int p = 17;
                        for(int a = 0; a < 10; a++)
                        {

                            for (int b = 0; b < c; b++)
                            {

                                Console.Write(" ");
                            }
                            c--;
                            for (int d = 0; d < e; d++)
                            {

                                Console.Write("#");
                            }
                            e += 2;
                            Console.Write("\n");
                        }
                        for (int q = 0; q < 9; q++)
                        {

                            for (int b = 0; b < o; b++)
                            {

                                Console.Write(" ");
                            }
                            o++;
                            for (int d = 0; d < p; d++)
                            {

                                Console.Write("#");
                            }
                            p -= 2;
                            Console.Write("\n");
                        }
                        break;

                    case 5:
                        int f = 15;
                        int g = 1;
                        int o11 = 1;
                        int p11 = 29;
                        for (int h = 0; h < 7; h++)
                        {

                            for (int l = 0; l < f; l++)
                            {

                                Console.Write(" ");
                            }
                            f -= 2;
                            for (int m = 0; m < g; m++)
                            {

                                Console.Write("@");
                            }
                            g += 4;
                            Console.Write("\n");
                        }
                        for (int q1 = 0; q1 < 7; q1++)
                        {

                            for (int b1 = 0; b1 < o11; b1++)
                            {

                                Console.Write(" ");
                            }
                            o11++;
                            for (int d1 = 0; d1 < p11; d1++)
                            {

                                Console.Write("@");
                            }
                            p11 -= 2;
                            Console.Write("\n");
                        }
                        break;

                    case 6:
                        int c1 = 4;
                        int e1 = 9;
                        int o1 = 1;
                        int p1 = 15;
                        for (int a1 = 7; a1 < 10; a1++)
                        {

                            for (int b1 = 0; b1 < c1; b1++)
                            {

                                Console.Write(" ");
                            }
                            c1--;
                            for (int d1 = 0; d1 < e1; d1++)
                            {

                                Console.Write("@");
                            }
                            e1 += 2;
                            Console.Write("\n");
                        }
                        for (int q1 = 0; q1 < 4; q1++)
                        {

                            for (int b1 = 0; b1 < o1; b1++)
                            {

                                Console.Write(" ");
                            }
                            o1++;
                            for (int d1 = 0; d1 < p1; d1++)
                            {

                                Console.Write("@");
                            }
                            p1 -= 2;
                            Console.Write("\n");
                        }
                        break;

                    case 7:
                        i = 7;
                        break;

                    default:
                        break;
                }
            }
        }
    }
}